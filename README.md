
# ifsc-calendar-api

A simple library to retrieve calendar information from the IFSC's unofficial web API.

See [the docs](https://docs.rs/ifsc-calendar-api) for more information.
